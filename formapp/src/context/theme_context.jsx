import React, { useState } from "react";
const data = {
  theme: "primary",
};

export const ContextTheme = React.createContext(data);

const { Provider } = ContextTheme;

const ProviderTheme = ({ _theme, children }) => {
  const [theme, set_theme] = useState(_theme);
  const toggle_theme = () => {
    set_theme((c) => (c === "primary" ? "dark" : "primary"));
  };

  return (
    <Provider
      value={{
        theme,
        toggle_theme,
      }}
    >
      {children}
    </Provider>
  );
};

export default ProviderTheme;
