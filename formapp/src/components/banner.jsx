import bannerImg from "../asset/img/avanzado.png";
export const Banner = () => {
  return (
    <div className="container-fluid py-1 mb-5">
      <img src={bannerImg} className="grande" alt="Responsive image" />
    </div>
  );
};
