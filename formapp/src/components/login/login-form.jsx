import { Field, Form, Formik } from "formik";
import Image from "../../asset/img/images1.jpeg";

const LoginForm = ({ onSubmit, innerRef }) => {
  const inicial_data = {
    username: "",
    password: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };
  return (
    <Formik initialValues={inicial_data} onSubmit={submit} innerRef={innerRef}>
      {({ isSubmitting }) => {
        return (
          <Form className="border p-4 form">
            <div className="text-center">
              <h3>Iniciar Sesion</h3>
            </div>
            <img src={Image} className="login" alt="Eniun " />
            <div className="form-group">
              <label className="form-check-label" htmlFor="username">
                User Name
              </label>
              <Field
                className="form-control form-control-lg "
                id="user_name"
                type="text"
                name="username"
              />
            </div>
            <div className="form-outline py-3 mb-2 ">
              <label className="form-label" htmlFor="username">
                Password
              </label>
              <Field
                className="form-control form-control-lg"
                id="pass"
                type="password"
                name="password"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};
export default LoginForm;
