export const ContactItems = (first_name, last_name, email, avatar) => {
  return (
    <div className="col-3">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">
            {first_name} - {last_name}
          </h5>

          <p class="card-text">
            <small class="text-muted">{email}</small>
          </p>
        </div>
        <img src={avatar} class="card-img-bottom" alt="..." />
      </div>
    </div>
  );
};
