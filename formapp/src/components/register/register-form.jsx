import { Formik, Form, Field } from "formik";
import Image from "../../asset/img/registrar.jpg";

const RegisterForm = ({ onSubmit, innerRef }) => {
  const data = {
    name: "",
    lastname: "",
    username: "",
    email: "",
    password: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm(true);
    actions.setSubmitting(false);
  };

  return (
    <Formik initialValues={data} onSubmit={submit} innerRef={innerRef}>
      {({ isSubmitting }) => {
        return (
          <Form className="border p-4 form">
            <div className="text-center">
              <h3>Registrar Contacto</h3>{" "}
            </div>
            <img src={Image} className="contact" alt="Eniun " />
            <div className="form-group">
              <label className="form-check-label" htmlFor="name">
                First name
              </label>
              <Field
                className="form-control form-control-lg "
                id="f_name"
                type="text"
                name="name"
              />
            </div>
            <div className="form-group">
              <label className="form-check-label" htmlFor="username">
                Last name
              </label>
              <Field
                className="form-control form-control-lg "
                id="last_name"
                type="text"
                name="lastname"
              />
            </div>
            <div className="form-group">
              <label className="form-check-label" htmlFor="username">
                User name
              </label>
              <Field
                className="form-control form-control-lg "
                id="user_name"
                type="text"
                name="username"
              />
            </div>
            <div className="form-group">
              <label className="form-check-label" htmlFor="username">
                Email
              </label>
              <Field
                className="form-control form-control-lg "
                id="e_mail"
                type="email"
                name="email"
              />
            </div>
            <div className="form-group">
              <label className="form-check-label" htmlFor="username">
                Password
              </label>
              <Field
                className="form-control form-control-lg "
                id="pass"
                type="password"
                name="password"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};
export default RegisterForm;
