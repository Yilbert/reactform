import { Link } from "react-router-dom";
import Imagen from "../../asset/img/peque.png";
import Imagen1 from "../../asset/img/images.jpeg";
export const Header = () => {
  return (
    <nav className="navbar navbar-expand- navbar-custom ">
      <div className="container">
        <Link className="navbar-brand" to="/">
          <img src={Imagen} className="pequeña" alt="Eniun " /> React.Js
          Avanzado
        </Link>

        <Link className="navbar-brand" to="/about">
          <h5>About </h5>
        </Link>

        <Link className="navbar-brand" to="/contacto">
          <h5> Contact</h5>
        </Link>

        <Link className="navbar-brand" to="/login">
          <img src={Imagen1} className="pequeña" alt="Eniun " /> Login
        </Link>
      </div>
    </nav>
  );
};
