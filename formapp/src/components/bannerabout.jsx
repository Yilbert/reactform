import bannerImg from "../asset/img/about.png";
export const Bannerabout = () => {
  return (
    <div className="container-fluid py-1 mb-5">
      <img src={bannerImg} className="grande" alt="Responsive image" />
    </div>
  );
};
