import "./App.css";
import { Index } from "./pages";
import Login from "./pages/login";
import { About } from "./pages/about";
import Register from "./pages/contact";

import { Header } from "./components/nav-bar/header";
import { Footer } from "./components/footer/footer";
import { Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import ProviderTheme from "./context/theme_context";
import { create } from "./services/instancia";

function App() {
  create("yilbert", "developer")
    .then((response) => {
      console.log("Result create", response?.data);
    })
    .catch((e) => {
      console.log("Error create", { ...e });
    });

  return (
    <ProviderTheme _theme={"primary"}>
      <div className="App">
        <Header />

        <Routes>
          <Route path="/" element={<Index />} />
          <Route path="/Login" element={<Login />} />
          <Route path="/contacto" element={<Register />} />
          <Route path="/about" element={<About />} />
        </Routes>
        <Footer />
      </div>
    </ProviderTheme>
  );
}

export default App;
