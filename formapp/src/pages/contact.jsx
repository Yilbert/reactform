import React, { useContext, useRef } from "react";
import RegisterForm from "../components/register/register-form";
import { ContextTheme } from "../context/theme_context";

const Register = () => {
  const ref = useRef(null);
  const context = useContext(ContextTheme);

  const register = (values) => {
    console.log(values);
  };
  return (
    <div className="abs-center mb-5 my-5">
      <div className="row aling-items-center">
        <div className="container"></div>
        <div className="col">
          <RegisterForm onSubmit={register} innerRef={ref} />
          <div className="d-grid gap-2">
            <button
              className={"btn btn-" + context.theme + " w-100 my-4 mb-3"}
              onClick={() => {
                ref.current.submitForm();
                context.toggle_theme("dark");
              }}
            >
              Sing In
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Register;
