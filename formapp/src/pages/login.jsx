import React, { useContext, useRef } from "react";

import LoginForm from "../components/login/login-form";
import { ContextTheme } from "../context/theme_context";

const Login = () => {
  const ref = useRef(null);
  const context = useContext(ContextTheme)

  const login = (values) => {
    console.log(values);
  };

  return (
    <div className="abs-center mb-5 my-5">
      
      <div className="row aling-items-center">
        <div className="container"></div>
        <div className="col">
          <LoginForm onSubmit={login} innerRef={ref} />
          <div classname="d-grid gap-2">
            <button
              className={"btn btn-"+context.theme+" w-100 my-4 mb-3"}
              onClick={() => {
                ref.current.submitForm();
                context.toggle_theme("dark")
              }}
            >
              Enter
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Login;
