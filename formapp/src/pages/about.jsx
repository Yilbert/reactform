import { Bannerabout } from "../components/bannerabout";

import {DetailUser} from "./detail_user"

export const About = () => {
  return (
    <div>
      <Bannerabout />
      
      <DetailUser/>
    </div>
  );
};
