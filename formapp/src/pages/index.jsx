import { Banner } from "../components/banner";
export const Index = () => {
  return (
    <div>
      <Banner />
      <h2>¿Que es ReactJS?</h2>

      <div className="row py-5">
        <div className="col">
          <h2>Intro</h2>
          <br />
          <p className="parrafo">
            ReactJS es otro de los framework imprescindibles que debes conocer.
            Desarrollado por Facebook, ReactJS es una biblioteca JavaScript de
            código abierto muy extendida. Los desarrolladores lo utilizan
            principalmente en la creación de interfaces de usuario y ocuparse de
            esta capa de visualización de una manera mucho más ágil, versátil y
            rápido que otros frameworks como por ejemplo AngularJS. También es
            una alternativa sólida para crear tanto aplicaciones web SPA de una
            sola página como incluso aplicaciones para móviles. Para ello,
            alrededor de React existe un completo ecosistema de módulos,
            herramientas y componentes capaces de ayudar al desarrollador a
            cubrir objetivos avanzados con relativamente poco esfuerzo.
          </p>
        </div>
      </div>
      <div className="row py-5">
        <div className="col">
          <h2>¿Por qué debería aprender ReactJS?</h2>
          <br />
          <p className="parrafo">
            React se puede utilizar para crear todo tipo de aplicaciones web,
            para móviles, e interfaces y mucho más. ReactJS es un framework
            JavaScript moderno, declarativo y eficiente que permite un
            desarrollo flexible y hace que la creación de interfaces de usuario
            interactivas/front-end sea divertida y completamente indolora. Es
            también un framework con un amplio respaldo en la comunidad debido a
            que cuenta con Facebook como su principal impulsor. Debido a esto es
            muy fácil encontrar documentación o buscar ayuda gracias a la
            comunidad en redes sociales (Facebook, Google, Twitter, LinkedIn,
            GitHub, Stackoverflow, Reddit, etc.). Debido a que Facebook está
            detrás de ReactJS su creación se realizó en base a unas necesidades
            concretas, derivadas del desarrollo de la web de la popular red
            social pero con el tiempo muchas otras aplicaciones web de primer
            nivel la fueron adoptando. Así nombres como BBC, Airbnb, Netflix,
            Dropbox y un largo etc hacen uso intensivo de ReactJS.
          </p>
        </div>
      </div>
      <div className="row py-5">
        <div className="col">
          <h2>Conclusion</h2>
          <br />
          <p className="parrafo">
            Esto es solo una pequeña muestra de lo que podemos hacer en muy poco
            tiempo. Cabe señalar que React es válido para hacer componentes que
            se pueden usar dentro de aplicaciones ya existentes lo que aumenta
            sus capacidades. ReactJS está en todas partes. Negar su importancia
            en estos momentos es casi como negar la propia existencia de la web.
            Cada vez más desarrollos utilizan este entorno de trabajo como base
            para sus proyectos y si deseas actualizar tus conocimientos, sin
            duda comenzar a trabajar con React es el camino a seguir.
          </p>
        </div>
      </div>
    </div>
  );
};
